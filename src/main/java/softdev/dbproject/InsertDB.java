/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.dbproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author EliteCorps
 */
public class InsertDB {
    public static void main(String[] args) {
        //Connect DB
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connect to database success");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //Insert DB
        String insert = "INSERT INTO category(category_id, category_name) VALUES (?, ?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(insert);
            stmt.setInt(1, 4);
            stmt.setString(2, "Test");
            int status = stmt.executeUpdate();
//            ResultSet key = stmt.getGeneratedKeys();
//            key.next();
//            System.out.println("" + key.getInt(1));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //Close DB
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
