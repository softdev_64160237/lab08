/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.dbproject.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import softdev.dbproject.database.Database;
import softdev.dbproject.model.User;

/**
 *
 * @author EliteCorps
 */
public class UserDao implements Dao<User> {
    
    public User getByName(String name) {
        User user = null;
        String select = "SELECT * FROM user WHERE user_name=?";
        Connection conn = Database.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(select);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user = new User();
                user.setId(rs.getInt("user_id"));
                user.setName(rs.getString("user_name"));
                user.setPassword(rs.getString("user_password"));
                user.setGender(rs.getString("user_gender"));
                user.setRole(rs.getInt("user_role"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;
    }

    @Override
    public User get(int id) {
        User user = null;
        String select = "SELECT * FROM user WHERE user_id=?";
        Connection conn = Database.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(select);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user = new User();
                user.setId(rs.getInt("user_id"));
                user.setName(rs.getString("user_name"));
                user.setPassword(rs.getString("user_password"));
                user.setGender(rs.getString("user_gender"));
                user.setRole(rs.getInt("user_role"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;
    }

    @Override
    public List<User> getAll() {
        ArrayList<User> list = new ArrayList<>();
        String select = "SELECT * FROM user";
        Connection conn = Database.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(select);

            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("user_id"));
                user.setName(rs.getString("user_name"));
                user.setPassword(rs.getString("user_password"));
                user.setGender(rs.getString("user_gender"));
                user.setRole(rs.getInt("user_role"));
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public User save(User obj) {
        String insert = "INSERT INTO user(user_name, user_password, user_gender, user_role)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = Database.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(insert);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getPassword());
            stmt.setString(3, obj.getGender());
            stmt.setInt(4, obj.getRole());
            int ret = stmt.executeUpdate();
            int id = Database.getInsertId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public User update(User obj) {
        String update = "UPDATE user "
                + " SET user_name = ?, user_password = ?, user_gender = ?, user_role = ?"
                + " WHERE user_id = ?";
        Connection conn = Database.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(update);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getPassword());
            stmt.setString(3, obj.getGender());
            stmt.setInt(4, obj.getRole());
            stmt.setInt(5, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(User obj) {
        String delete = "DELETE FROM user WHERE user_id=?";
        Connection conn = Database.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(delete);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();

            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
