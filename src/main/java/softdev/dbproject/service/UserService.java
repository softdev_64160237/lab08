/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.dbproject.service;

import softdev.dbproject.dao.UserDao;
import softdev.dbproject.model.User;

/**
 *
 * @author EliteCorps
 */
public class UserService {
    public User login(String name, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByName(name);
        if (user != null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }
}
