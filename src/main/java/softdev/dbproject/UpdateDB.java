/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.dbproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author EliteCorps
 */
public class UpdateDB {
    public static void main(String[] args) {
        //Connect DB
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connect to database success");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //Update DB
        String insert = "UPDATE category SET category_name = ? WHERE category_id = ?";
        try {
            PreparedStatement stmt = conn.prepareStatement(insert);
            stmt.setString(1, "test");
            stmt.setInt(2, 4);
            int status = stmt.executeUpdate();
//            ResultSet key = stmt.getGeneratedKeys();
//            key.next();
//            System.out.println("" + key.getInt(1));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //Close DB
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
