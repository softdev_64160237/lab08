/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.dbproject;

import softdev.dbproject.model.User;
import softdev.dbproject.service.UserService;

/**
 *
 * @author EliteCorps
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("user1", "pass@1234");
        if (user != null) {
            System.out.println("Welcome " + user.getName());
        } else {
            System.out.println("Error");
        }
    }
}
