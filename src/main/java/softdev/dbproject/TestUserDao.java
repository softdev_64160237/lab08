/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.dbproject;

import softdev.dbproject.dao.UserDao;
import softdev.dbproject.database.Database;
import softdev.dbproject.model.User;

/**
 *
 * @author EliteCorps
 */
public class TestUserDao {
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        System.out.println("Test getAll");
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }
        
        System.out.println("");
        System.out.println("Test get");
        User getUser = userDao.get(1);
        System.out.println(getUser);
        
//        System.out.println("");
//        System.out.println("Test insert");
//        User newUser = new User("user4", "pass@1234", "F", 2);
//        User insertUser = userDao.save(newUser);
//        System.out.println(insertUser);
        
        System.out.println("");
        System.out.println("Test update");
        getUser.setGender("M");
        userDao.update(getUser);
        User updateUser = userDao.get(getUser.getId());
        System.out.println(updateUser);
        
        
        System.out.println("");
        System.out.println("Test delete");
        userDao.delete(userDao.get(5));
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }
        
        Database.close();
    }
}
